# oaf oc.po
# Copyright (C) 2000-2002 Free Software Foundation, Inc.
#
# Yannig MARCHEGAY (yannig@marchegay.org) - 2006-2007
#
msgid ""
msgstr ""
"Project-Id-Version: oaf 0.6.10\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2002-05-23 01:26+0200\n"
"PO-Revision-Date: 2007-07-29 15:57+0200\n"
"Last-Translator: Yannig MARCHEGAY <yannig@marchegay.org>\n"
"Language-Team: Occitan (post 1500) <ubuntu-l10n-oci@lists.ubuntu.com>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n > 1);"

#: liboaf/oaf-async-corba.c:186
#: liboaf/oaf-fork-server.c:83
#, c-format
#: liboaf/oaf-fork-server.c:138
msgid "No server corresponding to your queryFailed to read from child process: %s\nChild process did not give an error message, unknown failure occurred"
msgstr ""

#: liboaf/oaf-fork-server.c:270
msgid "Couldn't fork a new process"
msgstr ""

#: liboaf/oaf-fork-server.c:291
#, c-format
#: liboaf/oaf-fork-server.c:297
#, c-format
#: liboaf/oaf-fork-server.c:355
#, c-format
#: liboaf/oaf-fork-server.c:366
#, c-format
#: liboaf/oaf-mainloop.c:221
#: liboaf/oaf-mainloop.c:223
#: liboaf/oaf-mainloop.c:223 oafd/main.c:72
msgid "Child received signal %u (%s)Unknown non-exit error (status is %u)OAF failed to set process group of %s: %s\nFailed to execute %s: %d (%s)\nObject directory to use when registering serversFile descriptor to print IOR onFD"
msgstr ""

#: liboaf/oaf-mainloop.c:225
#: liboaf/oaf-mainloop.c:227
#: liboaf/oaf-mainloop.c:438
msgid "IID to activatePrevent registering of server with OAFOAF options"
msgstr ""

#: liboaf/oaf-plugin.c:99
#, c-format
#: liboaf/oaf-plugin.c:116 liboaf/oaf-plugin.c:152
#, c-format
#: liboaf/oaf-plugin.c:203
#, c-format
#: liboaf/oaf-plugin.c:220
#, c-format
#: oafd/ac-corba.c:480
#: oafd/ac-corba.c:606
#: oafd/ac-corba.c:664
#: oafd/ac-corba.c:692
#: oafd/ac-corba.c:1118
msgid "g_module_open of `%s' failed with `%s'Can't find symbol OAF_Plugin_info in `%s'Factory '%s' returned NIL for `%s'Shlib '%s' didn't contain `%s'Couldn't find which child the server was listed inNothing matched the requirements.Query failed: Activation failed: Could not parse AID"
msgstr ""

#: oafd/ac-corba.c:1128
#: oafd/ac-corba.c:1152
#: oafd/main.c:62
#: oafd/main.c:62
#: oafd/main.c:64
msgid "Could not parse context: Could not activate server: Directory to read .oaf files fromDIRECTORYDomain of ObjectDirectory"
msgstr ""

#: oafd/main.c:64
msgid "DOMAIN"
msgstr "DOMENI"

#: oafd/main.c:67
msgid "Serve as an ActivationContext (default is as an ObjectDirectory only)"
msgstr ""

#: oafd/main.c:72
#: oafd/main.c:77
#: oafd/main.c:77
msgid "File descriptor to write IOR toQuery expression to evaluateEXPRESSION"
msgstr ""

#: oafd/od-activate.c:62
msgid "We don't handle activating shlib objects in a remote process yet"
msgstr ""

#.
#. * If this exception blows ( which it will only do with a multi-object )
#. * factory, you need to ensure you register the object you were activated
#. * for [use const char *bonobo_activation_iid_get (void); ] is registered
#. * with bonobo_activation_active_server_register - _after_ any other
#. * servers are registered.
#.
#: oafd/od-corba.c:302
#, c-format
#: oafd/od-corba.c:586
#, c-format
msgid "Race condition activating server '%s'"
"Couldn't find activation record for server `%s'. The likely cause is a "
"missing or incorrectly installed .oaf file."
msgstr ""

#: oafd/od-load.c:121 oafd/od-load.c:151
#, c-format
#: oafd/od-load.c:171
msgid "Property '%s' has no valuea NULL iid is not valid"
msgstr ""

#: oafd/od-load.c:175
#, c-format
msgid "iid %s has a NULL type"
msgstr ""

#: oafd/od-load.c:179
#, c-format
msgid "iid %s has a NULL location"
msgstr ""

#: oafd/od-load.c:188
#, c-format
#: oafd/od-utils.c:45
#, c-format
msgid "invalid character '%c' in iid '%s'"
"The OAF configuration file was not read successfully. Please, check it is "
"valid in: %s"
msgstr ""

#: oafd/OAF_naming-service.oaf.in.h:1
msgid "CORBA CosNaming service."
msgstr ""

#: oafd/OAF_naming-service.oaf.in.h:2
msgid "Name service"
msgstr ""

#: utils/oaf-sysconf.c:65
msgid "Could not save OAF configuration file.\n"
msgstr ""

#: utils/oaf-sysconf.c:66
msgid ""
"Please, make sure you have permissions to write OAF configuration file.\n"
msgstr ""

#: utils/oaf-sysconf.c:69
#: utils/oaf-sysconf.c:81
#, c-format
msgid "Successfully wrote OAF configuration file.\n"
"OAF configuration file is:\n"
"    %s\n"
msgstr ""

#: utils/oaf-sysconf.c:108
#, c-format
#: utils/oaf-sysconf.c:184
#: utils/oaf-sysconf.c:216
#: utils/oaf-sysconf.c:216 utils/oaf-sysconf.c:219
#: utils/oaf-sysconf.c:219
#: utils/oaf-sysconf.c:222
#: utils/oaf-sysconf.c:225
msgid "%s already in OAF configuration file\nOAF configuration file contains:\nDirectory to remove from OAF configuration filedirectory pathDirectory to add to OAF configuration fileDisplay directories in OAF configuration fileDisplay path to OAF configuration file"
msgstr ""

#  liboaf/oaf-registration.c:394
#~ msgid "Exec failed: %d (%s)\n"

#~ msgid "Trying dir %s\n"

#~ msgid "IID '%s' contains illegal characters; discarding\n"
